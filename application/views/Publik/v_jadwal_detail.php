<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body class="gray-bg">
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>#beranda">Beranda</a></li>
					<li><a href="<?php echo base_url(); ?>#jadwal">Jadwal</a></li>
					<li><a href="<?php echo base_url(); ?>#sarana">Sarana Olahraga</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a href="<?php echo base_url(); ?>#" ><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk/Daftar</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<section  class="section blue-bg">
		<div class="container">
			<div class="row title text-center">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<select class="form-control margin-top col-md-6" id="cabor">
						<option>Pilih Cabang Olah Raga</option>
						<?php foreach($cabor as $c){
							echo '<option value="'.$c['Cabor_Kode'].'">'.$c['Cabor_Nama'].'</option>';
						}?>
					</select>
					<br>
					<input type="date" class="form-control" id="tanggal">
					<a onclick="return jdw()" class="btn btn-submit" style="margin-top: 10px">Lihat Jadwal</a>
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-12">
					<hr>
					<?php $hari 		= ['','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];?>
				<h2 style="color: white">Jadwal <?php echo $sor[0]['Cabor_Nama'].'<br>'.$hari[date('N',strtotime($tanggal))].', '.date('d/m/Y',strtotime($tanggal))?></h2>
				</div>
			</div>
			<div class="row" style="margin-top: 10px">
				<div class="col-md-12">
					<?php foreach($sor as $s){?>
						<center>
						<h4 style="color: white"><?php echo $s['Sor_Nama']?></h4>
						<table class="table table-bordered" style="width: 50%;color: white">
							<tr>
								<th style="text-align: center">Jam</th>
								<th style="text-align: center">Harga / Status</th>
							</tr>
							<?php
								$detail 	= $this->db->where('Sor_Kode',$s['Sor_Kode'])->where('Harga_Hari',$hari[date('N',strtotime($tanggal))])->get('ref_harga')->result_array();
								foreach($detail as $d){
									$cek 	= $this->db->where('Harga_Id',$d['Harga_Id'])
														->where('Booking_Main',$tanggal)
														->join('dat_booking','dat_booking_detail.Booking_Kode = dat_booking.Booking_Kode','left')
														->get('dat_booking_detail')->row('Detail_Id');
									if($cek){
										echo '<tr>
												<td style="text-align: center;background-color: red">'.$d['Harga_Jam'].'</td>
												<td style="text-align: center;background-color: red">Terisi</td>
											 </tr>';
									}else{
										echo '<tr>
												<td style="text-align: center">'.$d['Harga_Jam'].'</td>
												<td style="text-align: center">'.number_format($d['Harga_Biaya'],0,'.',',').'</td>
											 </tr>';
									}
								}
							?>
						</table>
						</center>
					<?php }?>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
	<script type="text/javascript">
		function jdw(){
			tanggal 	= $('#tanggal').val();
			cabor 		= $('#cabor').val();
			window.open('<?php echo base_url() ?>index.php/Publik/jadwal/'+cabor+'/'+tanggal);
		}
	</script>
</body>

</html>
