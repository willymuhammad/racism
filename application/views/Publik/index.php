<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body>
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>#beranda">Beranda</a></li>
					<li><a href="<?php echo base_url(); ?>#jadwal">Jadwal</a></li>
					<li><a href="<?php echo base_url(); ?>#sarana">Sarana Olahraga</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a href="<?php echo base_url(); ?>#" ><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk/Daftar</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<header id="beranda">
		<div class="container">
			<div class="table">
				<div class="header-text">
					<div class="row">
						<div class="col-md-12 text-center">
							<h3 class="light white">Selamat Datang Di</h3>
							<h1 class="white typed">Rahayu Sport Center</h1>
							<span class="typed-cursor"></span>
							</div>
							<?php if($this->ion_auth->logged_in() == 1){?>
							<a href="<?php echo base_url(); ?>index.php/Booking" class="btn btn-white-fill ripple" style="margin-top: 10px">Booking Sekarang</a>
							<?php }else{?>
							<a href="<?php echo base_url(); ?>index.php/Register" class="btn btn-white-fill ripple" style="margin-top: 10px">Daftar Sekarang</a>
							<?php }?>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="cut cut-top"></div>
		<div class="container">
			<div class="row intro-tables">
				<div class="col-md-4">
					<div class="intro-table intro-table-first">
						<h5 class="white heading">Jadwal Sarana Olah Raga</h5>
						<div class="owl-carousel owl-schedule bottom">
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Futsal</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Basket</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Voli</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Bulu Tangkis</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Tenis Meja</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
								<div class="schedule-row row">
									<div class="col-xs-6">
										<h5 class="regular white">Panahan</h5>
									</div>
									<div class="col-xs-6 text-right">
										<h5 class="white">07:00 - 23.00</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-hover">
						<h5 class="white heading">Gym</h5>
						<div class="owl-testimonials bottom">
						<div class="item">
							<h1 class="white heading content"><span id="gym">0</span><p style="color: white">Ruangan Efektif Untuk 20 Orang</p><span class="open-blink"></span></h1>
							<h5 class="white heading light author">Jumlah Pengunjung</h5>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="intro-table intro-table-third">
						<h5 class="white heading">Kolam Renang</h5>
						<div class="owl-testimonials bottom">
							<div class="item">
								<h1 class="white heading content"><span id="kolam">0</span><span class="open-blink"></span></h1>
								<h5 class="white heading light author">Jumlah Pengunjung</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section section-padded blue-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="owl-twitter owl-carousel">
						<div class="item text-center">
							<i class="icon fa fa-blind"></i>
							<h4 class="white light">Rahayu Sport Center Kp. Pengkolan RT04/08 , Ds. Cikidang, Lembang, Bandung Barat</h4>
							<h4 class="light-white light"><a href="https://www.google.co.id/maps/place/Jl.+Pengkolan,+Lembang,+Kabupaten+Bandung+Barat,+Jawa+Barat+40391/@-6.813864,107.6524403,17z/data=!3m1!4b1!4m5!3m4!1s0x2e68e0902c3b5acf:0x9f0cbeab2457e8e9!8m2!3d-6.811916!4d107.656868
							" target="_blank"><button class="btn btn-info active" ><b " style="color: black">Lokasi !</b></button></a></h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-twitter"></i>
							<h4 class="white light">Cara Pembayaran </h4>
							<h4 class="light-white light">Setelah melakukan proses booking harap transfer ke No. BCA (No. Rek: 731 025 2527)
							Mandiri (No. Rek: 0700 000 899 992)
							BNI (No. Rek: 023 827 2088)
							BRI (No. Rek: 034 101 000 743 303)</h4>
						</div>
						<div class="item text-center">
							<i class="icon fa fa-envelope"></i>
							<h4 class="white light">Contact</h4>
							<h4 class="light-white light">
							Hp - 089656606665
							<br><a href="<?php echo base_url(); ?>http://www.gmail.com"  style="color: black" target="_blank" ><button class="btn btn-info active" ><b>Email - Yasserrisvandi@gmail.com</b></a></h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="jadwal" class="section section-padded">
		<div class="container">
			<div class="row text-center title">
				<h2>Jadwal</h2>
				<h4 class="light muted">Salam Olahraga!</h4>
				<a href="<?php echo base_url(); ?>index.php/Publik/jadwal/all/all" class="btn btn-blue" style="margin-top: 10px">Lihat Jadwal</a>
			</div>
			<div class="row services">
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/heart-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">FUTSAL</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/FS/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/guru-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">BOLA BASKET</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/BS/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/weight-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">BOLA VOLI</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/BV/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/weight-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">BULU TANGKIS</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/BT/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/weight-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">PANAHAN</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/PH/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
				<div class="col-md-3">
					<div class="service">
						<div class="icon-holder">
							<img src="<?php echo base_url(); ?>assets/img/icons/weight-blue.png" alt="" class="icon">
						</div>
						<h4 class="heading">TENIS MEJA</h4>
						<p class="description"><a href="<?php echo base_url()?>index.php/Publik/jadwal/TM/<?php echo date('Y-m-d')?>" class="btn btn-blue-fill">Lihat Jadwal</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="cut cut-bottom"></div>
	</section>
	<section id="sarana" class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Sarana Olahraga</h2>
				<h4 class="light muted">Sensasi Ber Olahraga Di Pegunungan</h4>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/futsal.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 80.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>FUTSAL</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<button data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill">Booking Sekarang</button>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/bulu_tangkis.jpeg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 14.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>BULU TANGKIS</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/renang.jpeg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 20.000</h3>
								<h5 class="light light-white">/ Orang</h5>
							</div>
						</div>
						<div class="title">
							<h4>RENANG</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Beli Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/panahan.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 30.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>PANAHAN</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/voli.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 50.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>BOLA VOLI</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/basket.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 50.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>BOLA BASKET</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/tenis_meja.jpg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 20.000</h3>
								<h5 class="light light-white">/ Jam</h5>
							</div>
						</div>
						<div class="title">
							<h4>TENIS MEJA</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
				<div class="col-md-3">
					<div class="team text-center">
						<div class="cover" style="background:url('<?php echo base_url()?>foto/gym.jpeg'); background-size:cover;">
							<div class="overlay text-center">
								<h3 class="white">Rp 10.000</h3>
								<h5 class="light light-white">/ Hari</h5>
							</div>
						</div>
						<div class="title">
							<h4>GYM</h4>
							<h5 class="muted regular">-</h5>
						</div>
						<a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue-fill ripple">Booking Sekarang</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup">
				<a href="<?php echo base_url(); ?>#" class="close-link"><i class="icon_close_alt2"></i></a>
				<h3 class="white">Selamat Datang</h3>
				<form action="<?php echo base_url()?>index.php/auth/login" method="post" class="popup-form">
					<input name="identity" type="text" class="form-control form-white" placeholder="Username">
					<input name="password" type="password" class="form-control form-white" placeholder="Password">
					<button type="submit" class="btn btn-submit">Masuk</button>
					<a href="<?php echo base_url() ?>index.php/Register" class="btn btn-submit">Daftar</a>
				</form>
			</div>
		</div>
	</div>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Selamat Ber Olahraga</h3>
					<h5 class="light regular light-white">"Mereka yang merasa tidak punya waktu untuk berolahraga, cepat atau lambat harus meluangkan waktu untuk merawat penyakitnya." — Edward Stanley</h5>
				</div>
				<div class="col-sm-6 text-center-mobile">
					<h3 class="white">Opening Hours <span class="open-blink"></span></h3>
					<div class="row opening-hours">
						<div class="col-sm-6 text-center-mobile">
							<h5 class="light-white light">Senin - Minggu</h5>
							<h3 class="regular white">07:00 - 22:00</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row bottom-footer text-center-mobile">
				<div class="col-sm-8">
					<p>&copy; Rahayu Sport Center Lembang</p>
				</div>
				<div class="col-sm-4 text-right text-center-mobile">
					<ul class="social-footer">
						<li><a href="<?php echo base_url(); ?>http://www.facebook.com/pages/Codrops/159107397912"><i class="fa fa-facebook"></i></a></li>
						<li><a href="<?php echo base_url(); ?>http://www.twitter.com/codrops"><i class="fa fa-twitter"></i></a></li>
						<li><a href="<?php echo base_url(); ?>https://plus.google.com/101095823814290637419"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script type="text/javascript">
		 setInterval(function() {
		      $.ajax({
					url: "<?php echo base_url()?>index.php/Publik/get_angka", 
					success: function(result){
						obj = JSON.parse(result)
	        			$("#kolam").text(obj['kolam']);
	        			$("#gym").text(obj['gym']);
	    			}
	    		});
		 }, 1000);
	</script>
</body>

</html>
