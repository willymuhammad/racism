<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body>
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>index.php/Pemesanan">Pemesanan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Ticketing">Ticketing</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Pembayaran">Pembayaran</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<section  class="section blue-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top" style="color: white">Pembelian Tiket</h2>
			</div>
			<div class="row">
				<div class="coll-md-6">
				<form action="<?php echo base_url()?>index.php/Ticketing/simpan" method="post" class="popup-form">
					<select id="jenis" name="jenis" class="form-control select" required="">
						<option value="0">-Pilih-</option>
						<?php foreach($cabor as $c){
							echo '<option value="'.$c['Cabor_Kode'].'">'.$c['Cabor_Nama'].'</option>';
						} 
						?>
					</select>
					<input id="jumlah" name="jumlah" type="number" class="form-control form-white" placeholder="Jumlah" required="" min="1" readonly="">
					<input id="harga" type="text"  type="text" class="form-control form-white" placeholder="Harga Satuan" readonly="">
					<input id="total_bayar" name="total_bayar" type="text" class="form-control form-white" placeholder="Total Bayar" readonly="">
					<button id="simpan" type="submit" class="btn btn-submit" disabled="" onclick="return save()">Simpan</button>
				</form>
				</div>	
			</div>
		</div>
	</section>
	
	<section id="sarana" class="section gray-bg">
			<div class="row">
				<div class="col-md-3"> </div>
				<div class="col-md-6">
					<table class="table table-bordered table-stripped">
						<thead>
							<tr>
								<th>Trans Kode</th>
								<th>Jenis</th>
								<th>Harga</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($tstatus as $t){?>
							<tr>
								<td><?php echo $t['Trans_Kode']; ?></td>
								<td><?php echo $t['Sor_Nama']; ?></td>
								<td><?php echo $t['Trans_Total']; ?></td>
								<td class="text-center"><a href="<?php echo base_url()?>index.php/Ticketing/hapus/<?php echo $t['Trans_Kode']; ?>"  class="btn btn-hapus" onclick="return confirm('Yakin Hapus ?')"><i class="fa fa-close"></i></a></td>
							</tr>
						<?php } ?>
							
						</tbody>
					</table>
				</div>
				<div class="col-md-3"> </div>
			</div>
		</section>
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

	<script type="text/javascript">
		$('#jenis').on('change',function(){
			var jenis = $('#jenis').val(); 
			if (jenis!='0'){
				$('#jumlah').attr('readonly',false);
				$('#simpan').attr('disabled',false);
				$('#total_bayar').val("");
				$('#jumlah').val("");
				$.ajax({
					url: "<?php echo base_url()?>index.php/Ticketing/get_harga/"+jenis, 
					success: function(result){
	        			$("#harga").val(result);
	    			}
	    		});
			}else{
				$('#jumlah').attr('readonly',true);
				$('#simpan').attr('disabled',true);
				$('#total_bayar').val("");
				$('#jumlah').val("");
				$('#harga').val("");
			}
			
		});
		$('#jumlah').keyup(function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});
		$('#jumlah').on('change',function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});

		function save(){
			if($('#jumlah').val() && $('#harga').val() && $('#total_bayar').val()){
				alert('Berhasil Simpan!');
				window.open('<?php echo base_url()?>index.php/Ticketing','_blank');
			}
		}

	
	</script>
</body>

</html>
