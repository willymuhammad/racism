<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		@media print 
		{
		   @page
		   {
		    size: 210mm 210mm; /* landscape */
 		  }
		}

		.break { page-break-before: always; }
		table{
			border-collapse: collapse;
		}
		table, td, th {
		    border: 1px solid black;
		}
	</style>
</head>
<body onload="print()">
<?php foreach ($trans as $t) {?>
<table width="100%">
	<tr>
		<td width="1%"><img width="150px;" src="<?php echo base_url().'qr/'.$t['Trans_Kode'].'.png' ?>"></td>
		<td>
			<p style="text-align: right">Kode : <i><?php echo $t['Trans_Kode'] ?></i></p>		
			<h3 style="text-align: center;margin-top: -20px">TIKET MASUK<br><?php echo $cabor?></h3>
			<h2 style="text-align: center;margin-top: -20px">RAHAYU SPORT</h2>
			<h3 style="text-align: center;margin-top: -25px">Rp. <?php echo number_format($t['Trans_Harga'],0,',','.') ?></h3>
			<p style="text-align: center;margin-top: -20px"><i>Berlaku Untuk Satu Orang. Satu Kali Masuk</i></p>
		</td>
	</tr>
</table>
<br>
<hr class="break" style="border-color: #ffffff">
<?php }?>
</body>
</html>