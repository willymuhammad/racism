<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bukti Bayar</title>
    
    <style>



    .invoice-box {
        max-width: 800px;
        margin: auto;
        /*padding: 30px;*/
        /*border: 1px solid #eee;*/
        /*box-shadow: 0 0 10px rgba(0, 0, 0, .15);*/
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body onload="print()">
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0" border="1">
            <tr class="top">
                <td colspan="5">
                    <table>
                        <tr>
                            <td style="text-align: left">
                                <img src="<?php echo base_url()?>Logo.png" align="left">
                                <h4 style="margin-top: 10px;margin-left: 100px;">Laporan Keuangan Rahayu Sport Center<br>Per Cabang Olah Raga Periode<br>
                                <?php echo date('d/m/Y',strtotime($awal))?> s/d <?php echo date('d/m/Y',strtotime($akhir))?></h4>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="heading" style="margin-top: 0px;">
                <td style="text-align: center;">#</td>
                <td style="text-align: center;">Cabang Olah Raga</td>
                <td style="text-align: center;">Jumlah</td>
                <td style="text-align: center;">Satuan</td>
                <td style="text-align: center;">Total(Rp)</td>
            </tr>
            <?php $total = 0;?>
            <?php $no = 1;?>
            <?php foreach($laporan as $l){?>
            <?php 
                  $data     = $this->db->where('Trans_Tanggal >=',$awal)
                                       ->where('Trans_Tanggal <=',$akhir)
                                       ->where('Trans_Status',2)
                                       ->where('ref_cabor.Cabor_Kode',$l['Cabor_Kode'])
                                       ->join('ref_sor','dat_trans.Sor_Kode = ref_sor.Sor_Kode')
                                       ->join('ref_cabor','ref_sor.Cabor_Kode = ref_cabor.Cabor_Kode')
                                       ->group_by('Trans_Satuan,Cabor_Nama')
                                       ->select('Cabor_Nama, SUM(Trans_Durasi) as Durasi,Trans_Satuan,SUM(Trans_Total) as Total')
                                       ->get('dat_trans')->row();
                    $satuan     = $this->db->where('Cabor_Kode',$l['Cabor_Kode'])->get('ref_cabor')->row('Cabor_Tipe');
            ?>
            <?php 
                if($data) $total += $data->Total;
            ?>
                <tr class="item">
                    <td style="text-align: center;"><?php echo $no++?></td>
                    <td style="text-align: left;"><?php echo $l['Cabor_Nama'] ?></td>
                    <td style="text-align: center"><?php if($data) echo $data->Durasi; else echo 0; ?></td>
                    <td style="text-align: center"><?php if($satuan == 'B') echo 'Jam';else echo 'Orang';?></td>
                    <td style="text-align: right"><?php if($data) echo number_format($data->Total,0,'.',','); else echo 0;?></td>
                </tr>
            <?php }?>
            
            
            
            <tr>
                <td colspan="4" colspan="5" style="text-align: right">Total</td>
                <td><strong><?php echo number_format($total,0,'.',',')?></strong></td>
            </tr>
        </table>
    </div>
</body>
</html>
