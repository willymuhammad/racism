<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body class="blue-bg">
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>">Beranda</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Booking">Booking</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Booking/daftar">Histori</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<form action="<?php echo base_url() ; ?>index.php/Booking/simpan_ganti" method="post">
	<input type="hidden" name="tanggal" value="<?php echo $tanggal ?>">
	<input type="hidden" name="cabor" value="<?php echo $caborview->Cabor_Kode ?>">
	<input type="hidden" name="sor" value="<?php echo $lapangview->Sor_Kode ?>">
	<input type="hidden" name="kode" value="<?php echo $kode ?>">
	<section  class="section blue-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top" style="color:white;">Form Ganti Jadwal</h2>
				<center>
				<table class="table" style="width: 25%">
					<tr>
						<td>Tanggal</td>
						<td><?php echo $hari." , ".date_format(date_create($tanggal),"d/m/Y"); ?></td>
					</tr>
					<tr>
						<td>Cabor</td>
						<td><?php echo $caborview->Cabor_Nama; ?></td>
					</tr>
					<tr>
						<td>Lapang</td>
						<td><?php echo $lapangview->Sor_Nama; ?></td>
					</tr>
				</table>
				</center>
			</div>
			<?php foreach($harga as $h){?>
			<?php 
			$check= $this->db->where('Harga_Id',$h['Harga_Id'])->get('dat_booking_detail')->row('Booking_Kode');
						?>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
				<?php if($check){?>
				<table class="table table-bordered table-stripped" style="background-color: #ccc">
				<?php }else{?>
					<table class="table table-bordered table-stripped">
					<?php }?>
						<thead>
							<tr>
							<?php if($check){?>

								<td valign="top"> <h5><?php echo $h['Sor_Nama']?> (Sudah dipesan)</h5> <h4 style="color:white;"style="color:white;"><?php echo $h['Harga_Jam']?></h4></td>
								<?php }else{?>
								<td valign="top"> <h5><?php echo $h['Sor_Nama']?></h5> <h4 style="color:white;"style="color:white;"><?php echo $h['Harga_Jam']?></h4></td>
							<?php }?>
								

								<td width="20%" valign="middle"><b style="color:white;">Rp <?php echo number_format($h['Harga_Biaya'],0,'.',',')?></b></td>
								<td width="5%">
									<label class="containers"> 
									  <input name="cb[]" type="checkbox" value="<?php echo$h['Harga_Id'] ?>" <?php if($check){ echo "disabled";}?> class='cb' onclick="return max_booking()">
									  <span class="checkmark" style="margin-left: 5px;"></span>
									</label> 
									<!-- <span style="width: 10px;"><i class="fa fa-close" style="color: red;"></i></span> -->
								</td>
							</tr>
						</thead>
					</table>
				</div>	
				<div class="col-md-3"></div>
			</div>
			<?php } ?>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<button id="bk" type="submit" class="btn btn-submit" disabled >Booking Sekarang</button>
				</div>	
				<div class="col-md-3"></div>
			</div>
		</div>
	</section>
	</form>
	
	
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

	<script type="text/javascript">
		

		function max_booking(){
			max=<?php echo $max ?>;
			checked = $('input:checkbox:checked').length;
			if(max==checked){
				$('input:checkbox:not(":checked")').prop('disabled',true);
			}
			else{
				$('input:checkbox:not(":checked")').prop('disabled',false);
			}
		}

		$('#jenis').on('change',function(){
			var jenis = $('#jenis').val(); 
			if (jenis!='0'){
				$('#cari').attr('disabled',false);
				$.ajax({
					url: "<?php echo base_url()?>index.php/Booking/get_lapang/"+jenis, 
					success: function(result){
	        			$("#lapang").html(result);
	    			}
	    		});
			}else{
				$('#jumlah').attr('readonly',true);
				$('#simpan').attr('disabled',true);
				$('#total_bayar').val("");
				$('#jumlah').val("");
				$('#harga').val("");
			}
			
		});
		$('#jumlah').keyup(function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});
		$('#jumlah').on('change',function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});

		$('.cb').on('change',function(){
			$('#bk').prop('disabled',false);
		})
	</script>
</body>

</html>
