<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body class="blue-bg">
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>">Beranda</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Booking">Booking</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Booking/daftar">Histori</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<section  class="section blue-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top" style="color:white;">Form Konfirmasi Pemabayaran</h2>
				<center>
				<table class="table" style="width: 50%;color: white;font-weight: bold">
					<tr>
						<td>Kode Booking</td>
						<td><?php echo $booking->Booking_Kode?></td>
					</tr>
					<tr>
						<td>Tanggal Booking</td>
						<td><?php echo $booking->Booking_Sekarang?></td>
					</tr>
					<tr>
						<td>Cabor</td>
						<td><?php echo $booking->Cabor_Nama?></td>
					</tr>
					<tr>
						<td>Lapang</td>
						<td><?php echo $booking->Sor_Nama?></td>
					</tr>
					<tr>
						<td>Total Harga</td>
						<td>Rp <?php echo number_format($booking->Booking_Total,0,',','.')?></td>
					</tr>
					<tr>
						<td>DP Yang Harus di Bayar Sejumlah</td>
						<td>Rp <?php echo number_format($booking->Booking_Dp,0,',','.')?></td>
					</tr>
				</table>
				<h4 class="white light">Cara Pembayaran </h4>
							<h4 class="light-white light" style="color: white"><b>Pembayaran dapat dilakukan melalui transfer dana ke salah satu rekening di bawah ini : <br>BCA (No. Rek: 731 025 2527)<br>Mandiri (No. Rek: 0700 000 899 992)<br>BNI (No. Rek: 023 827 2088)<br>BRI (No. Rek: 034 101 000 743 303)</b></h4>
				*DP Di bayar 50% Dari Total Harga<hr>
				<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<h3 class="margin-top" style="color:white;margin-top: 5px">Batas Pembayaran</h3>
					<h3 class="margin-top" style="color:white;margin-top: 5px" id='waktu'>00:00:00</h3>
					<h3 class="margin-top" style="color:white;margin-top: 5px">Upload Bukti Transfer</h3>
					<form action="<?php echo base_url() ; ?>index.php/Booking/upload" method="post" enctype="multipart/form-data">
					<input  name="file" type="file" class="form-control form-white" placeholder="Upload" required="" min="1" accept="image/*" required="">
					<input type="hidden" name="kode" value="<?php echo $booking->Booking_Kode?>">
					<br>
					<input  name="Jumlah_Bayar" type="number" class="form-control form-white" placeholder="Jumlah DP (Minimal <?php echo number_format($booking->Booking_Dp,0,',','.') ?> )" required="" min="<?php echo $booking->Booking_Dp ?>" Max="<?php echo $booking->Booking_Total ?>" required="">
					<button id="simpan" type="submit" class="btn btn-submit" ">Upload</button>
					</form>
				</div>
				<div class="col-md-3"></div>
				</div>
				</center>
			</div>
		</div>
	</section>
	
	
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

	<script type="text/javascript">
		
	var countDownDate = new Date('<?php echo $booking->Booking_Batas ?>').getTime();

    var x = setInterval(function() {
      var now = new Date().getTime();
      var distance = countDownDate - now;
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      if(hours < 10){
      	hour = '0'+hours;
      }else{
      	hour = hours;
      }
      if(minutes < 10){
      	minute = '0'+minutes;
      }else{
      	minute = minutes;
      }
      if(seconds < 10){
      	second = '0'+seconds;
      }else{
      	second = seconds;
      }
      $('#waktu').html("<b>"+hour+":"+minute+ ":"+second+"</b>");
      if (distance <= 0) {
        clearInterval(x);
        alert('habis waktu');
         window.location.href="<?php echo base_url()?>index.php/Booking/batal/<?php echo $booking->Booking_Kode?>"
      }
    }, 1000);
	</script>
</body>

</html>
