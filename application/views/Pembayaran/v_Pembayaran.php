<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body class="gray-bg">
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>index.php/Pemesanan">Pemesanan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Ticketing">Ticketing</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Pembayaran">Pembayaran</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<section  class="section gray-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top">Pembayaran</h2>
				<a style="margin-top: -60px" href="<?php echo base_url()?>index.php/Pembayaran/Offline" class="btn btn-blue pull-right" >Bayar Offline</a>
			</div>
			<div class="row" style="margin-top: 10px">
				<div class="col-md-12">
					<?php if($this->session->flashdata('msg')){ ?>
					<div class="alert alert-success">
					 <?php echo $this->session->flashdata('msg'); ?>
					</div>				
					<?php } ?>
					<table class="table table-bordered table-stripped">
						<thead>
							<tr>
								<th>Kode Booking</th>
								<th>Pengunjung</th>
								<th>Cabor/Sor</th>
								<th>Tanggal/Jam</th>
								<th>Total Bayar</th>
								<th>Dp</th>
								<th>Bukti Bayar/Sisa Bayar</th>
								<th>Aksi</th>

							</tr>
						</thead>
						<tbody>
						<?php foreach($pembayaran as $p){?>
							<tr>
								<td><?php echo $p['Booking_Kode']; ?></td>
								<td><?php 
										if($p['level'] == 1){
											echo '-';
										}else{
											echo $p['first_name'];
										}
									?>
								</td>
								<td><?php echo $p['Cabor_Nama']; ?></td>
								<td>
									<?php 
										$kode 	= $this->db->where('Booking_Kode',$p['Booking_Kode'])
														   ->join('ref_harga','dat_booking_detail.Harga_Id = ref_harga.Harga_Id')
														   ->get('dat_booking_detail')->result_array();
										echo $kode[0]['Harga_Hari'].", ".$p['Booking_Main'];
										echo "<br>";
										foreach($kode as $kd){
											echo $kd['Harga_Jam'];
											echo "<br>";
										}
									?>
								</td>
								<td>Rp <?php echo number_format($p['Booking_Total'],0,',','.'); ?></td>
								<td>Rp <?php echo number_format($p['Booking_Dp'],0,',','.'); ?></td>
								<td>
									<?php 
									if($p['Booking_Status']==1){
										echo '<img width="50px" src="'.base_url().'upload/'.$p['Booking_Kode'].'.jpg">';
									}
									else{
										echo 'Rp '.number_format($p['Booking_Total']-$p['Booking_Dp'],0,',','.');
									}
									?>
								</td>
								<td>
									<?php 
									if($p['Booking_Status']==2){
										echo '<a style="margin-top: 0" href="'.base_url().'index.php/Pembayaran/Main/'.$p['Booking_Kode'].'" class="btn btn-submit" >Main Sekarang</a>';
									}
									else{
										echo '<a style="margin-top: 0" href="'.base_url().'index.php/Pembayaran/Lunas/'.$p['Booking_Kode'].'" class="btn btn-submit" target="_blank" onclick="return bukti(\''. $p['Booking_Kode'].'\')">Lunas</a>';
									}
									?>
								</td>
							</tr>
						<?php } ?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
	<script type="text/javascript">
		function bukti(kode){
			window.location.href="<?php echo base_url()?>index.php/Pembayaran/bukti/"+kode;
		}
	</script>
</body>

</html>
