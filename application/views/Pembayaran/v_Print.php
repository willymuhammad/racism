<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bukti Bayar</title>
    
    <style>



    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body onload="print()">
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0" border="1">
            <tr class="top">
                <td colspan="5">
                    <table>
                        <tr>
                            <td>
                                <img src="<?php echo base_url()?>Logo.png" style="width:50%; max-width:100px;" align="left">
                                <h3 style="text-align: left;"><i>Nota Pembayaran</i></h3>
                            </td>
                            <td style="text-align: right;">
                                Booking Kode : <?php echo $booking->Booking_Kode ?><br>
                                Tanggal Booking : <?php echo $booking->Booking_Sekarang ?><br>
                                Tanggal Main : <?php echo $booking->Booking_Main ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        
            
       
            
            <tr class="heading">
                <td>
                    Cabor/Sor  
                </td>
                <td style="text-align: center;">
                    Jam
                </td>
                <td style="text-align: center;">
                    Jumlah Jam
                </td>
                <td style="text-align: right;">
                    Harga
                </td>
                <td style="text-align: right;">
                    Total
                </td>
            </tr>
            
            <tr class="item">
                <td>
                    <?php echo $booking->Cabor_Nama ?>
                </td>
                <td style="text-align: center;">
                    <?php 
                        $data = $this->db->where('Booking_Kode',$booking->Booking_Kode)->join('ref_harga','ref_harga.Harga_Id=dat_booking_detail.Harga_Id')->get('dat_booking_detail')->result_array(); 
                        foreach($data as $d){
                            echo $d['Harga_Jam'].'<br>';
                        }
                    ?>
                </td >
                <td style="text-align: center;">
                    <?php echo $this->db->where('Booking_Kode',$booking->Booking_Kode)->count_all_results('dat_booking_detail') ?>
                </td >
                <td style="text-align: right;">
                	Rp <?php echo number_format($booking->Booking_Total / $this->db->where('Booking_Kode',$booking->Booking_Kode)->count_all_results('dat_booking_detail'),0,',','.') ?>
                </td>
                <td style="text-align: right;">
                    Rp <?php echo number_format($booking->Booking_Total,0,',','.') ?>
                </td>
            </tr>
            
            
            
            <tr>
                <td colspan="4">Total</td>
                
                <td>
                   <strong>Rp <?php echo number_format($booking->Booking_Total,0,',','.') ?></strong>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
