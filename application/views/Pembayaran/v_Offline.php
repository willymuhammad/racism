<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RACISM</title>
	<meta name="description" content="Cardio is a free one page template made exclusively for Codrops by Luka Cvetinovic" />
	<meta name="keywords" content="html template, css, free, one page, gym, fitness, web design" />
	<meta name="author" content="Luka Cvetinovic for Codrops" />
	<!-- Favicons (created with http://realfavicongenerator.net/)-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicons/apple-touch-icon-60x60.png">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicons/manifest.json">
	<link rel="shortcut icon" href="<?php echo base_url(); ?>Logo.png">
	<meta name="msapplication-TileColor" content="#00a8ff">
	<meta name="msapplication-config" content="<?php echo base_url(); ?>assets/img/favicons/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<!-- Normalize -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/normalize.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<!-- Owl -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/owl.css">
	<!-- Animate.css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/animate.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!-- Elegant Icons -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/eleganticons/et-icons.css">
	<!-- Main style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cardio.css">
</head>

<body class="blue-bg">
	<div class="preloader">
		<img src="<?php echo base_url(); ?>assets/img/loader.gif" alt="Preloader image">
	</div>
	<nav class="navbar" style="opacity: 1">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>#"><img width="50%" src="<?php echo base_url(); ?>Logo.png" data-active-url="Logo.png" alt="" style="margin-top: -25px;"></a>
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right main-nav">
					<li><a href="<?php echo base_url(); ?>index.php/Pemesanan">Pemesanan</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Ticketing">Ticketing</a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Pembayaran">Pembayaran</a></li>
					<?php if($this->ion_auth->logged_in() == 1){?>
					<li><a><?php echo $this->ion_auth->get_user_id()->first_name?></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/Publik/logout" >Keluar</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url(); ?>#" data-toggle="modal" data-target="#modal1" class="btn btn-blue">Masuk</a></li>
					<?php }?>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<section  class="section blue-bg">
		<div class="container">
			<div class="row title text-center">
				<h2 class="margin-top" style="color: white">Pembayaran Offline</h2>
			</div>
			<div class="row">
				<div class="coll-md-6">
				<form action="<?php echo base_url()?>index.php/Pembayaran/cari" method="post" class="popup-form">
				<input id="date" name="date" type="date" class="form-control form-white" placeholder="Pilih Tanggal Booking" required="" min="<?php echo date('Y-m-d')?>">
					<select id="jenis" name="jenis" class="form-control select" required="">
						<option value="0">-Pilih Cabang Olah Raga-</option>
						<?php foreach($cabor as $c){
							echo '<option value="'.$c['Cabor_Kode'].'">'.$c['Cabor_Nama'].'</option>';
						} 
						?>
					</select>
					<select id="lapang" name="lapang" class="form-control select" required="">
					</select>
					<button id="cari" type="submit" class="btn btn-submit" disabled="" >Cari</button>
				</form>
				</div>	
			</div>
			<hr>
			<div class="row title text-center">
				<h2 class="margin-top" style="color: white">Pembayaran Jangka Panjang</h2>
			</div>
			<div class="row">
				<div class="coll-md-6">
				<form action="<?php echo base_url()?>index.php/Pembayaran/jangkapanjang" method="post" class="popup-form">
					<select id="jenis_" name="jenis_" class="form-control select" required="">
						<option value="0">-Pilih Cabang Olah Raga-</option>
						<?php foreach($cabor as $c){
							echo '<option value="'.$c['Cabor_Kode'].'">'.$c['Cabor_Nama'].'</option>';
						} 
						?>
					</select>
					<select id="lapang_" name="lapang_" class="form-control select" required="">
					</select>
					<table width="100%" class="table table-bordered" style="color: white">
					<?php 
						$awal = 7;
						$akhir = 8;
						for($i = 7; $i < 23;$i++){
							if($awal < 10) $awal = '0'.$awal;
							if($akhir < 10) $akhir = '0'.$akhir;
							echo '<tr>
								<td style="height:50px;text-align:center">'.$awal.':00-'.$akhir.':00</td>
								<td width="1%"><label class="containers"> 
									  <input class="cb" name="cb[]" type="checkbox" value="'.$awal.'.00-'.$akhir.'.00">
									  <span class="checkmark" style="margin-left: 5px;"></span>
									</label> </td>
							</tr>';
							$awal = $awal*1+1;
							$akhir = $akhir*1+1;
						}
					?>
					</table>
					<h3 style="color: white">Dari Tanggal</h3>
					<input name="date_awal" type="date" class="form-control form-white" placeholder="Pilih Tanggal Booking" required="" min="<?php echo date('Y-m-d')?>">
					<h3 style="color: white">Sampai Tanggal</h3>
					<input name="date_akhir" type="date" class="form-control form-white" placeholder="Pilih Tanggal Booking" required="" min="<?php echo date('Y-m-d')?>">
					<button id="cari_" type="submit" class="btn btn-submit" disabled="" >Simpan</button>
				</form>
				</div>	
			</div>
		</div>
	</section>
	
	
	
	<!-- Holder for mobile navigation -->
	<div class="mobile-nav">
		<ul>
		</ul>
		<a href="<?php echo base_url(); ?>#" class="close-link"><i class="arrow_up"></i></a>
	</div>
	<!-- Scripts -->
	<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/typewriter.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.onepagenav.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

	<script type="text/javascript">
		$('#jenis').on('change',function(){
			var jenis = $('#jenis').val(); 
			if (jenis!='0'){
				$('#cari').attr('disabled',false);
				$.ajax({
					url: "<?php echo base_url()?>index.php/Booking/get_lapang/"+jenis, 
					success: function(result){
	        			$("#lapang").html(result);
	    			}
	    		});
			}else{
				$('#jumlah').attr('readonly',true);
				$('#simpan').attr('disabled',true);
				$('#total_bayar').val("");
				$('#jumlah').val("");
				$('#harga').val("");
			}
			
		});

		$('#jenis_').on('change',function(){
			var jenis = $('#jenis_').val(); 
			if (jenis!='0'){
				$('#cari_').attr('disabled',false);
				$.ajax({
					url: "<?php echo base_url()?>index.php/Booking/get_lapang/"+jenis, 
					success: function(result){
	        			$("#lapang_").html(result);
	    			}
	    		});
			}else{
				$('#jumlah_').attr('readonly',true);
				$('#simpan_').attr('disabled',true);
				$('#total_bayar_').val("");
				$('#jumlah_').val("");
				$('#harga_').val("");
			}
			
		});
		$('#jumlah').keyup(function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});
		$('#jumlah').on('change',function(){
			var jenis 	= $('#jenis').val();
			var jumlah 	= $('#jumlah').val();
			var harga 	= $('#harga').val();
			if (jenis== '0') {
				alert('Harap Pilih Jenis Olah Raga');
			}else{
				var	total	= jumlah*harga;
				$('#total_bayar').val('Rp. '+numeral(total).format('0,0'));	
			}
			
		});

	</script>
</body>

</html>
