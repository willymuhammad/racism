<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->library('email'); 
		$this->load->model('M_Register');    
		$this->load->model('M_Publik');    
		$this->load->model('M_Booking');    
		

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data	= array('cabor'		=> $this->M_Booking->get_Cabor()->result_array());
		$this->load->view('Jadwal/v_jadwal',$data);
	}

	public function harga($cabor){
		$data	= array('cabor'	=> $this->db->where('Cabor_Kode',$cabor)->get('ref_cabor')->row(),
						'sor'	=> $this->M_Publik->get_jadwal($cabor)->result_array());
		$this->load->view('Jadwal/v_harga',$data);
	}

	public function ubah(){
		$harga 	= $this->input->post('harga');
		$id 	= $this->input->post('id');
		$cabor 	= $this->input->post('cabor');
		$this->db->where('Harga_Id',$id)->update('ref_harga',array('Harga_Biaya'=>$harga));
		redirect('Jadwal/harga/'.$cabor);
	}

	public function ubahseluruh(){
		$harga 	= $this->input->post('harga_baru');
		$sor 	= $this->input->post('sor_kode');
		$cabor 	= $this->input->post('cabor_kode');
		for($i=0;$i<count($harga);$i++){
			$this->db->where('Sor_Kode',$sor[$i])->update('ref_harga',array('Harga_Biaya'=>$harga[$i]));
		}
		redirect('Jadwal/harga/'.$cabor);		
	}
}
