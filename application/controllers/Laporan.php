<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Publik'); 
		$this->load->model('M_Booking'); 
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$data 	= array('cabor' => $this->db->get('ref_cabor')->result_array());
		$this->load->view('Laporan/v_laporan',$data);
	}

	public function cabor($awal,$akhir){
		if(!$awal || !$akhir){
			redirect('Laporan');
		}else{
			$laporan 	= $this->db->get('ref_cabor')->result_array();
			$data 		= array('laporan'	=> $laporan,
								'awal'		=> $awal,
								'akhir'		=> $akhir);
			$this->load->view('Laporan/v_periode',$data);
		}
	}

	public function sor($awal,$akhir){
		if(!$awal || !$akhir){
			redirect('Laporan');
		}else{
			$laporan 	= $this->db->get('ref_sor')->result_array();
			$data 		= array('laporan'	=> $laporan,
								'awal'		=> $awal,
								'akhir'		=> $akhir);
			$this->load->view('Laporan/v_sor',$data);
		}
	}

	public function tanggal($awal,$akhir,$cabor){
		if(!$awal || !$akhir){
			redirect('Laporan');
		}else{
			$laporan 	= $this->db->where('Trans_Tanggal >=',$awal)->where('Trans_Tanggal <=',$akhir)
								   ->where('Trans_Status',2)
								   ->where('ref_sor.Cabor_Kode',$cabor)
								   ->join('ref_sor','dat_trans.Sor_Kode = ref_sor.Sor_Kode')
								   ->join('ref_cabor','ref_sor.Cabor_Kode = ref_cabor.Cabor_Kode')
								   ->group_by('Trans_Tanggal ,Trans_Satuan,Cabor_Nama')
								   ->select('Trans_Tanggal, Cabor_Nama, SUM(Trans_Durasi) as Durasi, Trans_Satuan, SUM(Trans_Total) as Total')
								   ->get('dat_trans')->result_array();
			$nama 		= $this->db->where('Cabor_Kode',$cabor)->get('ref_cabor')->row('Cabor_Nama');
			$data 		= array('laporan'	=> $laporan,
								'cabor'		=> $nama,
								'awal'		=> $awal,
								'akhir'		=> $akhir);
			$this->load->view('Laporan/v_tanggal',$data);
		}
	}

}
