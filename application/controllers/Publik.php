<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publik extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Publik'); 
		$this->load->model('M_Booking'); 
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('Publik/index');
	}

	public function logout()
	{	
		$this->ion_auth->logout();
		return redirect('/', 'refresh');
	}

	public function get_angka(){
		$kolam	= $this->M_Publik->get_kolam()->result();
		$gym	= $this->M_Publik->get_gym()->result();
		$data	= array('kolam'=>count($kolam),
						'gym'=>count($gym));
		echo json_encode($data);
	}

	public function jadwal($cabor,$tanggal){
		if($cabor == 'all' && $tanggal == 'all'){
			$data	= array('sor'		=> $this->M_Publik->get_jadwal($cabor)->result_array(),
							'cabor'		=> $this->M_Booking->get_Cabor()->result_array());
			$this->load->view('Publik/v_jadwal',$data);
		}else{
			$data	= array('sor'		=> $this->M_Publik->get_jadwal($cabor)->result_array(),
							'cabor'		=> $this->M_Booking->get_Cabor()->result_array(),
							'tanggal'	=> $tanggal);
			$this->load->view('Publik/v_jadwal_detail',$data);
		}

	}
}
