<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembayaran extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Pembayaran');    
		$this->load->model('M_Booking');    
		$this->load->model('M_Ticketing');    
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$data	= array('pembayaran' => $this->M_Pembayaran->get_all()->result_array());
		$this->load->view('Pembayaran/v_Pembayaran',$data);

	}

	public function tanggal($tgl){
		$data	= array('pembayaran' => $this->M_Pembayaran->get_all_tanggal($tgl)->result_array());
		$this->load->view('Pembayaran/v_Pembayaran',$data);
	}

	public function Lunas($kode){
		$this->M_Pembayaran->Lunas($kode);
		$booking 	= $this->M_Booking->get_all_booking($kode);
		$kodes		= $this->M_Ticketing->get_kode($booking->Sor_Kode,$booking->Booking_Main);
		$tgl		=	date('dmy',strtotime($booking->Booking_Main));
		if ($kodes){
			$kodes 	= $kodes->Trans_Kode;
			$kodes 	= substr($kodes, 8,3)*1;
			$kodes 	= $kodes+1;
			if ($kodes<10)$kodes='00'.$kodes;
			elseif ($kodes<100)$kodes='0'.$kodes;
			$kodes   = 	$tgl.$booking->Cabor_Kode.$kodes;
		}else{
			$kodes   = 	$tgl.$booking->Cabor_Kode.'001';
		}
		$durasi 	= $this->db->where('BOOKING_KODE',$kode)->count_all_results('dat_booking_detail');
		$data 		= array('Trans_Kode'	=> $kodes,
							'Trans_Tanggal'	=> $booking->Booking_Main,
							'Trans_Harga'	=> $booking->Booking_Total / $durasi,
							'Trans_Durasi'	=> $durasi,
							'Trans_Satuan'	=> 'Jam',
							'Trans_Total'	=> $booking->Booking_Total,
							'Trans_Status'	=> 2,
							'Sor_Kode'		=> $booking->Sor_Kode);
		$this->db->insert('dat_trans',$data);
		$this->session->set_flashdata('msg', 'Pelunaasan Transaksi Kode '.$kode.' Berhasil');
		redirect('Pembayaran');
	}

	public function Offline(){
		$data	= array('cabor' => $this->M_Booking->get_Cabor()->result_array());
		$this->load->view('Pembayaran/v_Offline',$data);
	}

	public function Main($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>9));
		$this->session->set_flashdata('msg', 'Konfirmasi Main Kode '.$kode.' Berhasil');
		redirect('Pembayaran');
	}

	public function cari(){
		$tgl	=	$this->input->post('date');
		$cabor	=	$this->input->post('jenis');
		$lapang	=	$this->input->post('lapang');
		$day	=	date('w',strtotime($tgl));
		$hari	=	array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		$harga	=	$this->M_Booking->get_Harga($hari[$day],$lapang)->result_array();

		$data = array('harga' => $harga,
						'tanggal'=> $tgl,
						'cabor' => $cabor,
						'caborview' =>$this->M_Booking->cabor_nama($cabor)->row(),
						'lapangview' =>$this->M_Booking->lapang_nama($lapang)->row(),
						'lapang' => $lapang,
						'hari' => $hari[$day]);
		$this->load->view('Pembayaran/v_Cari',$data);
	}
	
	public function simpan(){
		$cb 	= $this->input->post('cb');
		$kode 	= $this->M_Booking->get_kode();

		if (empty($kode)) {
			$kode=date('ymd').'001';
		}else{
			$kode=substr($kode, 6,3)*1+1;
			if($kode<10)$kode=date('ymd').'00'.$kode;
			elseif($kode<100)$kode=date('ymd').'0'.$kode;
			else$kode=date('ymd').$kode;
		}

		$total = 0; 
		$jam = array();
		foreach ($cb as $cb) {
			$total+= $this->M_Booking->get_harga_jam($cb);
			$data  = array('Harga_Id' => $cb,
							'Booking_Kode' => $kode);
			$this->db->insert('dat_booking_detail',$data);
			array_push($jam, $this->M_Booking->get_jam($cb));
		}
		$data	= array('Booking_Kode' 	=> $kode,
						'Booking_Sekarang'=> date('Y-m-d'),
						'Booking_Main'=> $this->input->post('tanggal'),
						'Booking_Dp' => 0,
						'Booking_Total' => $total,
						'Booking_Status' => 2,
						'Cabor_Kode'	=> $this->input->post('cabor'),
						'Sor_Kode'	=> $this->input->post('sor'),
						'Id_User'	=> $this->ion_auth->get_user_id()->id);
		$this->db->insert('dat_booking',$data);
		$data = array('booking'=> $this->M_Booking->get_all_booking($kode),
			'jam' => $jam);
		$this->load->view('Pembayaran/v_Bayar',$data);

	}

	public function upload(){
		$kode = $this->input->post('kode');
		$dp   = $this->input->post('Jumlah_Bayar');
		$this->M_Pembayaran->update_dp($kode,$dp);
		redirect('Pembayaran');
	}

	public function bukti($kode){
		$data	= array('booking' 	=> $this->M_Pembayaran->bukti($kode));
		$this->load->view('Pembayaran/v_Print',$data);
	}

	public function jangkapanjang(){
		$cabor 	= $this->input->post('jenis_');
		$lapang = $this->input->post('lapang_');
		$cb 	= $this->input->post('cb');
		$awal 	= $this->input->post('date_awal');
		$akhir 	= $this->input->post('date_akhir');
		
		$hari	=	['','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'];

		$minggu = $this->datediff('ww', $awal, $akhir, false);
		$totalall 	= 0;
		for($i = 0;$i<$minggu;$i++){
			$total 	= 0;
			foreach($cb as $c){
				$id 	= $this->db->where('Harga_Jam',$c)->where('Harga_Hari',$hari[date('N',strtotime($awal))])->get('ref_harga')->row();
				$totalall 	+= $id->Harga_Biaya;
				$total		+= $id->Harga_Biaya;
			}
			$kode 	= $this->M_Booking->get_kode();
		
			if (empty($kode)) {
				$kode=date('ymd').'001';
			}else{
				$kode=substr($kode, 6,3)*1+1;
				if($kode<10)$kode=date('ymd').'00'.$kode;
				elseif($kode<100)$kode=date('ymd').'0'.$kode;
				else$kode=date('ymd').$kode;
			}

			if($i == 0 ) $main = $awal;
			else $main = date('Y-m-d',strtotime($main.'+7 days'));

			$data	= array('Booking_Kode' 		=> $kode,
							'Booking_Sekarang'	=> date('Y-m-d'),
							'Booking_Main'		=> $main,
							'Booking_Dp' 		=> $total,
							'Booking_Total' 	=> $total,
							'Booking_Status' 	=> 2,
							'Cabor_Kode'		=> $cabor,
							'Sor_Kode'			=> $lapang,
							'Id_User'			=> $this->ion_auth->get_user_id()->id);
			$this->db->insert('dat_booking',$data);
			foreach ($cb as $c) {
				$id 	= $this->db->where('Sor_Kode',$lapang)->where('Harga_Jam',$c)->where('Harga_Hari',$hari[date('N',strtotime($awal))])->get('ref_harga')->row();
				$data  = array('Harga_Id' => $id->Harga_Id,
								'Booking_Kode' => $kode);
				$this->db->insert('dat_booking_detail',$data);
			}
		}
		print_r($totalall);
	}

	function datediff($interval, $datefrom, $dateto, $using_timestamps = false){
	    /*
	    $interval can be:
	    yyyy - Number of full years
	    q    - Number of full quarters
	    m    - Number of full months
	    y    - Difference between day numbers
	           (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
	    d    - Number of full days
	    w    - Number of full weekdays
	    ww   - Number of full weeks
	    h    - Number of full hours
	    n    - Number of full minutes
	    s    - Number of full seconds (default)
	    */

	    if (!$using_timestamps) {
	        $datefrom = strtotime($datefrom, 0);
	        $dateto   = strtotime($dateto, 0);
	    }

	    $difference        = $dateto - $datefrom; // Difference in seconds
	    $months_difference = 0;

	    switch ($interval) {
	        case 'yyyy': // Number of full years
	            $years_difference = floor($difference / 31536000);
	            if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
	                $years_difference--;
	            }

	            if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
	                $years_difference++;
	            }

	            $datediff = $years_difference;
	        break;

	        case "q": // Number of full quarters
	            $quarters_difference = floor($difference / 8035200);

	            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
	                $months_difference++;
	            }

	            $quarters_difference--;
	            $datediff = $quarters_difference;
	        break;

	        case "m": // Number of full months
	            $months_difference = floor($difference / 2678400);

	            while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
	                $months_difference++;
	            }

	            $months_difference--;

	            $datediff = $months_difference;
	        break;

	        case 'y': // Difference between day numbers
	            $datediff = date("z", $dateto) - date("z", $datefrom);
	        break;

	        case "d": // Number of full days
	            $datediff = floor($difference / 86400);
	        break;

	        case "w": // Number of full weekdays
	            $days_difference  = floor($difference / 86400);
	            $weeks_difference = floor($days_difference / 7); // Complete weeks
	            $first_day        = date("w", $datefrom);
	            $days_remainder   = floor($days_difference % 7);
	            $odd_days         = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?

	            if ($odd_days > 7) { // Sunday
	                $days_remainder--;
	            }

	            if ($odd_days > 6) { // Saturday
	                $days_remainder--;
	            }

	            $datediff = ($weeks_difference * 5) + $days_remainder;
	        break;

	        case "ww": // Number of full weeks
	            $datediff = floor($difference / 604800);
	        break;

	        case "h": // Number of full hours
	            $datediff = floor($difference / 3600);
	        break;

	        case "n": // Number of full minutes
	            $datediff = floor($difference / 60);
	        break;

	        default: // Number of full seconds (default)
	            $datediff = $difference;
	        break;
	    }

	    return $datediff;
	}
}
