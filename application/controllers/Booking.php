<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Booking');    
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$data	= array('cabor' => $this->M_Booking->get_Cabor()->result_array());
		$this->load->view('Booking/v_Booking',$data);

	}

	public function get_lapang($jenis)
	{
		$lap 	=	$this->M_Booking->get_Sor($jenis)->result_array();
		$opt	=	'';
		foreach($lap as $l){
			$opt.='<option value="'.$l['Sor_Kode'].'">'.$l['Sor_Nama'].'</option>';
		}
		echo $opt;
	}

	public function cari(){
		$tgl	=	$this->input->post('date');
		$cabor	=	$this->input->post('jenis');
		$lapang	=	$this->input->post('lapang');
		$day	=	date('w',strtotime($tgl));
		$hari	=	array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		$harga	=	$this->M_Booking->get_Harga($hari[$day],$lapang)->result_array();

		$data = array('harga' => $harga,
						'tanggal'=> $tgl,
						'cabor' => $cabor,
						'caborview' =>$this->M_Booking->cabor_nama($cabor)->row(),
						'lapangview' =>$this->M_Booking->lapang_nama($lapang)->row(),
						'lapang' => $lapang,
						'hari' => $hari[$day]);
		$this->load->view('Booking/v_Cari',$data);
	}

	public function cari_ganti(){
		$tgl	=	$this->input->post('date');
		$cabor	=	$this->input->post('jenis');
		$lapang	=	$this->input->post('lapang');
		$kode	=	$this->input->post('kode');
		$day	=	date('w',strtotime($tgl));
		$hari	=	array('Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu');
		$harga	=	$this->M_Booking->get_Harga($hari[$day],$lapang)->result_array();

		$data = array('harga' => $harga,
						'tanggal'=> $tgl,
						'cabor' => $cabor,
						'caborview' =>$this->M_Booking->cabor_nama($cabor)->row(),
						'lapangview' =>$this->M_Booking->lapang_nama($lapang)->row(),
						'lapang' => $lapang,
						'kode' => $kode,
						'max' => $this->db->where('Booking_Kode',$kode)->count_all_results('dat_booking_detail'),
						'hari' => $hari[$day]);
		$this->load->view('Booking/v_Cari_Ganti',$data);
	}

	public function simpan(){
		$cb 	= $this->input->post('cb');
		$kode 	= $this->M_Booking->get_kode();

		if (empty($kode)) {
			$kode=date('ymd').'001';
		}else{
			$kode=substr($kode, 6,3)*1+1;
			if($kode<10)$kode=date('ymd').'00'.$kode;
			elseif($kode<100)$kode=date('ymd').'0'.$kode;
			else$kode=date('ymd').$kode;
		}

		$total = 0; 
		$jam = array();
		foreach ($cb as $cb) {
			$total+= $this->M_Booking->get_harga_jam($cb);
			$data  = array('Harga_Id' => $cb,
							'Booking_Kode' => $kode);
			$this->db->insert('dat_booking_detail',$data);
			array_push($jam, $this->M_Booking->get_jam($cb));
		}
		$data	= array('Booking_Kode' 	=> $kode,
						'Booking_Sekarang'=> date('Y-m-d'),
						'Booking_Main'=> $this->input->post('tanggal'),
						'Booking_Dp' => $total/2,
						'Booking_Total' => $total,
						'Booking_Status' => 0,
						'Booking_Batas' =>date('Y-m-d H:i:s',time()+$this->db->where('Opt_Nama','batas')->get('ref_opt')->row('Opt_Value')*60),
						'Cabor_Kode'	=> $this->input->post('cabor'),
						'Sor_Kode'	=> $this->input->post('sor'),
						'Id_User'	=> $this->ion_auth->get_user_id()->id);
		$this->db->insert('dat_booking',$data);
		$data = array('booking'=> $this->M_Booking->get_all_booking($kode),
			'jam' => $jam);
		$this->load->view('Booking/v_Konfirmasi',$data);

	}

	public function konfirmasi($kode){
		$data = array('booking'=> $this->M_Booking->get_all_booking($kode));
		$this->load->view('Booking/v_Bayar',$data);
	}

	public function simpan_ganti(){
		$cb 	= $this->input->post('cb');
		$kode 	= $this->input->post('kode');

		$this->M_Booking->delete_ganti($kode);
		$total = 0; 
		$jam = array();
		foreach ($cb as $cb) {
			$total+= $this->M_Booking->get_harga_jam($cb);
			$data  = array('Harga_Id' => $cb,
							'Booking_Kode' => $kode);
			$this->db->insert('dat_booking_detail',$data);
			array_push($jam, $this->M_Booking->get_jam($cb));
		}

		$data	= array('Booking_Main'=> $this->input->post('tanggal'),
						'Booking_Ganti'=>1,
						'Booking_Total' => $total + 5000);
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',$data);
		$data = array('booking'=> $this->M_Booking->get_all_booking($kode),
			'jam' => $jam);
		redirect('Booking/daftar');

	}

	public function upload(){
		$kode = $this->input->post('kode');
		$dp   = $this->input->post('Jumlah_Bayar');
		$config['upload_path']='./upload/';
		$config['allowed_types']='jpg|png';
		$config['file_name']=$kode.'.jpg';
		$this->load->library('upload',$config);
		if (!$this->upload->do_upload('file')) {
			print_r($this->upload->display_errors());
		}else{
			$this->M_Booking->upload($kode);
			$this->M_Booking->update_dp($kode,$dp);
			$this->session->set_flashdata('msg', 'Konfirmasi Pemesanan Berhasil, Cek Email Anda Untuk Menerima Konfirmasi Pembayaran');
			redirect('Booking/daftar');
		}
	}

	public function batal($kode){
		$this->M_Booking->batal($kode);
		redirect('Booking/daftar');
	}

	public function daftar(){
		$list = $this->M_Booking->daftar()->result_array();
		$data = array('booking'=> $list);
		$this->load->view('Booking/v_Daftar',$data);
	}

	public function ganti_jadwal($kode){
		$data	= array('booking' => $this->M_Booking->get_all_booking($kode));
		$this->load->view('Booking/v_ganti_jadwal',$data);
	}
}
