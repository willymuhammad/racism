<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticketing extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Ticketing');    
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$data	= array('cabor' 	=> $this->M_Ticketing->get_Cabor()->result_array(),
						'tstatus' 	=> $this->M_Ticketing->tstatus()->result_array());
		$this->load->view('Ticketing/index',$data);

	}

	public function simpan()
	{
		$tgl		=	date('dmy');
		$tgl2		=	date('Y-m-d');
		$cabor 		=	$this->input->post('jenis');
		$jumlah 	=	$this->input->post('jumlah');
		$sor		=	$this->M_Ticketing->get_Sor($cabor)->Sor_Kode;
		$harga 		=	$this->M_Ticketing->get_harga($sor)->Harga_Biaya;
		$print_kode =	array();
		$namacabor 	= $this->db->where('Cabor_Kode',$cabor)->get('ref_cabor')->row('Cabor_Nama');
		for ($i=0; $i <$jumlah; $i++) { 
			$kode		=	$this->M_Ticketing->get_kode($sor,$tgl2);
			if ($kode){
				$kode 	= $kode->Trans_Kode;
				$kode 	= substr($kode, 8,3)*1;
				$kode 	= $kode+1;
				if ($kode<10)$kode='00'.$kode;
				elseif ($kode<100)$kode='0'.$kode;
				$kode   = 	$tgl.$cabor.$kode;
			}else{
				$kode   = 	$tgl.$cabor.'001';
			}
			$data	   =	array('Trans_Kode'		=> 	$kode,
								  'Trans_Tanggal' 	=> 	$tgl2,
								  'Trans_Harga'		=> 	$harga,
								  'Trans_Durasi'	=> 	1,
								  'Trans_Satuan' 	=> 'Orang',
								  'Trans_Total'		=>	$harga,
								  'Trans_Status'	=>	0,
								  'Sor_Kode'		=>	$sor);
			array_push($print_kode, $kode);
			$this->db->insert('dat_trans', $data);
		}
		$this->print_tiket($print_kode,$namacabor);
		// $this->index()
	}	

	public function print_tiket($print_kode,$namacabor){
		$kode   = $this->M_Ticketing->get_Trans($print_kode)->result_array();
		$out	= array();
		$url 	= 'http://192.168.8.102/sor/';
		// $url 	= base_url();
		foreach($kode as $k){
			$this->load->library('ciqrcode');
			$params['data'] = $url .'index.php/Ticketing/update/'.$k['Trans_Kode'];
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'qr/'.$k['Trans_Kode'].'.png';
			$this->ciqrcode->generate($params);
		}
		$data	= array('trans' 	=> $kode,
						'cabor'		=> $namacabor);
		$this->load->view('Ticketing/print',$data);
	}

	public function update($kode){
		$pesan = $this->M_Ticketing->update($kode);
		echo '<center><h1>'.$pesan.'</h1></center>';
	}

	public function get_harga($id){
		$sor		=	$this->M_Ticketing->get_Sor($id)->Sor_Kode;
		$harga 		=	$this->M_Ticketing->get_harga($sor)->Harga_Biaya;
		echo $harga;
	}

	public function reset(){
		$reset		=	$this->M_Ticketing->reset();
		echo $reset;
	}

	public function hapus($h){
		$this->M_Ticketing->hapus($h);
		$this->index();
	}



}
