<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->library('email'); 
		$this->load->model('M_Register');    
		

    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('Register/v_Register');
	}

	public function simpan()
	{
		$nama 		=	$this->input->post('nama');
		$email 		=	$this->input->post('email');
		$password 	=	$this->input->post('password');
		$code 		=	$this->random(16);
		$password   = 	$this->ion_auth_model->hash_password($password,FALSE,FALSE);;
		$cek 		= 	$this->db->where('email',$email)->get('users')->row('id');
		if($cek){
			$this->session->set_flashdata('msg', 'Email Telah Tersedia');
		    $this->index();
		}else{
			$data	   =	array('password'		=> 	$password,
								  'email' 			=> 	$email,
								  'username' 		=> 	$email,
								  'active' 			=> 	0,
								  'activation_code'	=>	$code,
								  'level' 			=> 	2,
								  'first_name'		=> 	$nama);
			
			$this->db->insert('users', $data);
			$pesan= 'Registari anda telah berhasil Silahkan Klik dibawah ini untuk aktivasi akun anda : <br><br>
			<a href="'.base_url().'index.php/Register/activation?email='.$email.'&code='.$code.'">BUKA LINK</a>
			';
			$this->email->set_newline("\n");
			$this->email->from('info@racism.web.id', 'Admin SOR');
			$this->email->to($email);
			 
			$this->email->subject('Konfirmasi User');
			$this->email->message($pesan);
			if ( ! $this->email->send()) {
		        show_error($this->email->print_debugger());
		    } 
		    $this->session->set_flashdata('msg', 'Cek Email anda :)');
		    $this->index();
		}
	}

	function random($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function activation(){
		$email 	=	$this->input->get('email');
		$code 	=	$this->input->get('code');
		$this->M_Register->activation($email,$code);
		$this->session->set_flashdata('msg', 'Silahkan Login akun anda sudah aktif');
	    $this->index();


	}
}
