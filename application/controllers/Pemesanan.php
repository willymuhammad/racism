<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan extends CI_Controller {
	function __construct()
    {
        parent::__construct();
   		$this->load->add_package_path(APPPATH.'third_party/ion_auth/');
		$this->load->library('ion_auth'); 
		$this->load->model('M_Pemesanan');    
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		$data	= array('pemesanan' => $this->M_Pemesanan->get_all()->result_array());
		$this->load->view('Pemesanan/v_Pemesanan',$data);

	}

	public function Konfirmasi($kode){
		$this->M_Pemesanan->Konfirmasi($kode);

			$url 	= base_url();
			$this->load->library('ciqrcode');
			$params['data'] = base_url().'index.php/Pembayaran/main/'.$kode;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'qr/'.$kode.'.png';
			$this->ciqrcode->generate($params);

		$data = $this->db->where('Booking_Kode',$kode)->join('users','users.id=dat_booking.Id_User')->get('dat_booking')->row();
		$pesan= 'Proses Booking Berhasil dilakukan!<br>Silahkan datang pada hari dan jam main dan perlihatkan email ini kepada petugas kami<br>Kode Booking : '.$kode.'<br>Nama : '.$data->first_name.'<br><a href="'.base_url().'qr/'.$kode.'.png">Download QR Code</a>';
		$this->email->set_newline("\n");
		$this->email->from('info@racism.web.id', 'Admin SOR');
		$this->email->to($data->email);
		 
		$this->email->subject('Konfirmasi Pemesanan');
		$this->email->message($pesan);
		if ( ! $this->email->send()) {
	        show_error($this->email->print_debugger());
	    } 
	    $this->session->set_flashdata('msg', 'Konfirmasi Pemesanan Kode '.$kode.' Berhasil');
		redirect('Pemesanan');
	}

	public function Lunas($kode){
		$this->M_Pemesanan->Lunas($kode);
		redirect('Pemesanan');
	}
	
	public function setbooking(){
		$jam 	= $this->input->post('jam');
		$menit 	= $this->input->post('menit');
		$waktu 	= ($jam * 60) + $menit;
		$this->db->where('Opt_Nama','batas')->update('ref_opt',array('Opt_Value'=>$waktu));
		redirect('Pemesanan');
	}
}
