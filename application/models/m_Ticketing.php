<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Ticketing extends CI_Model{
	
	function get_Cabor(){
		$q	=	$this->db->where('Cabor_Tipe','T')->get('ref_cabor');
		return $q;
	}

	function get_Sor($cabor){
		$q	=	$this->db->where('Cabor_Kode', $cabor)->get('ref_sor');
		return $q->row();	
	}

	function get_kode($sor,$tgl2){
		$q	=	$this->db->where('Sor_Kode', $sor)
						 ->where('Trans_Tanggal',$tgl2)
						 ->order_by('Trans_Kode','desc')
						 ->get('dat_trans');
		return $q->row();	
	}

	function get_harga($sor){
		$q	=	$this->db->where('Sor_Kode', $sor)->get('ref_harga');
		return $q->row();	
	}	

	function get_Trans($print_kode){
		$q	=	$this->db->where_in('Trans_Kode', $print_kode)
						 ->get('dat_trans');
		return $q;	
	}

	function update($kode){

		$q		=	$this->db->where('Trans_Kode', $kode)
							 ->get('dat_trans')->row()->Trans_Status;
		if ($q == 0) {
			$data 	= array('Trans_Status'	=> 1);
			$pesan = "BERHASIL MASUK!";
		}else {
			$data  = array('Trans_Status'	=> 2);
			$pesan = "BERHASIL KELUAR!";
		}					 
		
		$this->db->where('Trans_Kode',$kode)->update('dat_trans',$data);
		return $pesan;
	}

	 function reset(){
		$data 	= array('Trans_Status' =>2);
		$this->db->where('Trans_Status',1)->update('dat_trans',$data);
		return "Reset BERHASIL";
	}

	function tstatus(){
		$q	=	$this->db->join('ref_sor','ref_sor.Sor_Kode = dat_trans.Sor_Kode')
						 ->where('Trans_Status',0)->get('dat_trans');
		return $q;
	}

	function hapus($h){
		$this->db->where('Trans_Kode',$h)->delete('dat_trans');
		
	}
}
