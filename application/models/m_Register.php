<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Register extends CI_Model{
	
	function activation($email,$code){
		$data 	= array('active'	=> 1);
		$this->db->where('email',$email)->where('activation_code',$code)->update('users',$data);
	}

}
