<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Booking extends CI_Model{
	
	function get_Cabor(){
		$q	=	$this->db->where('Cabor_Tipe','B')->get('ref_cabor');
		return $q;
	}

	function get_Sor($cabor){
		$q	=	$this->db->where('Cabor_Kode', $cabor)->get('ref_sor');
		return $q;	
	}

	function get_Harga($hari,$lapang){
		$q	=	$this->db->join('ref_sor','ref_harga.Sor_Kode=ref_sor.Sor_Kode')
						 ->where('Harga_Hari',$hari)
						 ->where('ref_harga.Sor_Kode',$lapang)
						 ->get('ref_harga');
		return $q;
	}
	
	function cabor_nama($cabor){
		$q	=	$this->db->where('Cabor_Kode',$cabor)->get('ref_cabor');
		return $q;
	}

	function lapang_nama($lapang){
		$q	=	$this->db->where('Sor_Kode',$lapang)->get('ref_sor');
		return $q;
	}

	function get_kode(){
		$q	=	$this->db->where('Booking_Sekarang',date("Y-m-d"))->order_by('Booking_Kode',"desc")->get('dat_booking')->row('Booking_Kode');
		return $q;
	}

	function get_harga_jam($id){
		$q = $this->db->where('Harga_Id',$id)->get('ref_harga')->row('Harga_Biaya');
		return $q;
	}

	function get_jam($id){
		$q = $this->db->where('Harga_Id',$id)->get('ref_harga')->row('Harga_Jam');
		return $q;
	}

	function get_all_booking($id){
		$q = $this->db->join('ref_cabor','dat_booking.Cabor_Kode=ref_cabor.Cabor_Kode')
					  ->join('ref_sor','dat_booking.sor_kode=ref_sor.sor_kode')
					  ->where('Booking_Kode',$id)->get('dat_booking')->row();
		return $q;
	}

	function upload($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>1));
	}

	function update_dp($kode,$dp){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Dp'=>$dp));
	}

	function batal($kode){
		$this->db->where('Booking_Kode',$kode)->delete('dat_booking');
		$this->db->where('Booking_Kode',$kode)->delete('dat_booking_detail');
	}

	function daftar(){
		$q= $this->db->where('id_user',$this->ion_auth->get_user_id()->id)
					 ->join('ref_cabor','dat_booking.Cabor_Kode=ref_cabor.Cabor_Kode')
					 ->join('ref_sor','dat_booking.sor_kode=ref_sor.sor_kode')
					 ->get('dat_booking');
		return $q;
	}

	function delete_ganti($kode){
		$this->db->where('Booking_Kode',$kode)->delete('dat_booking_detail');
	}

}
