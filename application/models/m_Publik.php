<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Publik extends CI_Model{
	
	function get_kolam(){
		$q	=	$this->db->where('Trans_Status', '1')
						->where('Sor_Kode','RN001')
						->get('dat_trans');
		return $q;	
	}

	function get_gym(){
		$q	=	$this->db->where('Trans_Status', '1')
						->where('Sor_Kode','GY001')
						->get('dat_trans');
		return $q;	
	}

	function get_jadwal($cabor){
		$q 	= $this->db->where('ref_sor.Cabor_Kode',$cabor)
						->join('ref_cabor','ref_sor.Cabor_Kode = ref_cabor.Cabor_Kode')
						->get('ref_sor');
		return $q;
	}

}
