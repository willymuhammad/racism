<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Pembayaran extends CI_Model{
	
	function get_all(){
		$q	=	$this->db->where_in('Booking_Status',[2,9])
						 ->where('Booking_Main',date('Y-m-d'))
						 ->join('users','dat_booking.Id_User=users.id')
						 ->join('ref_cabor','dat_booking.Cabor_Kode = ref_cabor.Cabor_Kode')
						 ->get('dat_booking');
		return $q;
	}

	function get_all_tanggal($tgl){
		$q	=	$this->db->where_in('Booking_Status',[2,9])
						 ->where('Booking_Main',$tgl)
						 ->join('users','dat_booking.Id_User=users.id')
						 ->join('ref_cabor','dat_booking.Cabor_Kode = ref_cabor.Cabor_Kode')
						 ->get('dat_booking');
		return $q;
	}

	function Lunas($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>3));
	}
	
	function Main($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>9));
	}

	function update_dp($kode,$dp){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Dp'=>$dp));
	}

	function bukti($kode){
		$q = $this->db->join('ref_cabor','dat_booking.Cabor_Kode=ref_cabor.Cabor_Kode')
					  ->join('ref_sor','dat_booking.sor_kode=ref_sor.sor_kode')
					  ->where('Booking_Kode',$kode)->get('dat_booking')->row();
		return $q;
	}
}
