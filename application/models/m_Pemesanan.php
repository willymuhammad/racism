<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Pemesanan extends CI_Model{
	
	function get_all(){
		$q	=	$this->db->where_in('Booking_Status',[1])
						 ->join('users','dat_booking.Id_User=users.id')
						 ->join('ref_cabor','dat_booking.Cabor_Kode = ref_cabor.Cabor_Kode')
						 ->order_by('Cabor_Nama')
						 ->get('dat_booking');
		return $q;
	}

	function Konfirmasi($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>2));
	}

	function Lunas($kode){
		$this->db->where('Booking_Kode',$kode)->update('dat_booking',array('Booking_Status'=>3));
	}


}
